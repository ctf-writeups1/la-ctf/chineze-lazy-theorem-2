import socket

def extended_euclidean_algorithm(a, b):
    """Solve function help me to compute the Euclidian
    algorithme of two numbers, for obtained PGCD of 
    the numbers, but also one Bezout coefficient, a pair 
    of u and v ints like au + bv = PGCD
    """
    if b == 0:
        return (a, 1, 0)
    gcd, u, v = extended_euclidean_algorithm(b, a % b)
    return (gcd, v, u - (a // b) * v)

def solve_congruence(a, b, m):
    """Solve Congruence function help me to find x modulo
    value in congruence equations like 7x ≡ -2 (mod 5),
    exemple : 7x ≡ -2 (mod 5) 
    <=> x ≡ 4 (mod 5)
    """
    gcd, x, v = extended_euclidean_algorithm(a, m)
    if b % gcd != 0:
        return None
    else:
        x = (x * b // gcd) % m
        return x

def chineze_remainder(system):
    """Chineze_remainder function help me to retrieve big numbers with 
    informations about these modulo. a system is composed by a tupple
    of pair of modulo and values, like ((Mod_A,A),(Mod_B,B)) for a
    system of two values.
    The return is a tupple, with first is the modulo_prod (Mod_A*Mod_B)
    and second is the value as x = value (mod modulo_prod)
    So if modulo_prod > x : x = value.
    """
    system=sorted(system, reverse=True)
    mod = system[0][0]
    r = system[0][1]
    for i in range(len(system)-1):
        j = solve_congruence(mod, system[i+1][1] - r, system[i+1][0])
        r = mod * j + r
        mod *= system[i+1][0]
    return mod, r

if __name__ == "__main__":
    ### Create server connection
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("lac.tf", 31111))

    ### Get Primes number from server
    primes = s.recv(1024).split()[:2]

    ### Get Remainds for the two Primes
    values = []
    for prime in primes:
        ### Send 1 for choose Ask a modulo
        s.sendall(b"1\n")
        data = s.recv(1024).split()
        ### Send Prime number
        s.sendall(prime+bytes("\n", "UTF-8"))
        ### Add result to the list
        values.append(int(s.recv(1024).split()[0]))
        
    ### Send 2 for choose guess number
    s.sendall(b"2\n")
    primes = list(map(int, primes))
    for i in range(30):
        ### Zip Modulos and Values
        system = zip((primes+[31]),(values+[i]))
        ### Compute X
        maximum, guess = chineze_remainder(list(system))
        ### Send Try
        s.sendall(bytes(str(guess),"UTF-8") + b"\n")
        data = s.recv(1024).split()
        if "lactf" in str(data[0]):
            # Here Come the Flag :)
            print(str(data[0]))
            break
        # print(i, data[0])


