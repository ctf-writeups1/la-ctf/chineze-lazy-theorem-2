# crypto/chinese-lazy-theorem-2    391pt

## Description

Ok I'm a little less lazy now but you're still not getting much from me.

nc lac.tf 31111

file : chinese-lazy-theorem-2.py

## Analyse

We again have a tcp serv at lac.tf, port 31111.

Reading the python, or connecting to the server, we know that, this time, to obtain the
flag from the server, we have to guess a random generated number between 1 and the product
of 2*3*5 and two primes numbers of 512 bit (We call it x).

At the begining of the connection, we receive the values for the two Big primes numbers.

New x and primes numbers are calculted for each new connections.

To guess x, we can ask to the server the euclidean remain of two number that we choose ourself.

Then, we have 30 try to guess the right number before the connection is closed.

Also, trying some values directly in the server is not an option, because it's seems like a timer close the connection automatically. We have to send the rights data fast.

## Method

The name of the challenge give clues about the method to obtain x : The Chinese remainder theorem

Reading wikipedia, we gather precious informations, like : 

if we know that the remainder of x divided by 3 is 2, the remainder of x divided by 5 is 3, and the remainder of x divided by 7 is 2, then without knowing the value of x, we can determine that the remainder of x divided by 105 (the product of 3, 5, and 7) is 23. Importantly, this tells us that if x is a natural number less than 105, then 23 is the only possible value of x, under the condition that the divisors are pairwise coprime (no two divisors share a common factor other than 1


With this sentence, we can determine that, choosing the rights numbers to ask, we can compute the truth value of x.

## Maths

The wikipedia exemple take the system :

    x % 3 == 1
    x % 5 == 4    < Python like personal notation
    x % 7 == 6

or :

    x ≡ 1 mod 3
    x ≡ 4 mod 5  < The Mathematical Notation
    x ≡ 6 mod 7

So, the chineze remainder theorem say that we can determine x with these clues. We proceed like that :

Let's begin with the biggest modulo. We define j a non-negativ number. Indeed, in world of modulos, x ≡ 6 mod 7 is exactly equivalent to the classic equation x = 6 + 7j, because, for all possibles values of j, the resulted modulo is correct.

        x ≡ 6 mod 7
    <=> x = 6 + 7j
We can know subsituted this equation in the second biggest system value : 

        x ≡ 4 mod 5
    <=> 6 + 7j ≡ 4 mod 5
Okay, know, we have to solve this congruence to obtain the remain value of j mod 5.
After some researchs :

        7j + 6 ≡ 4 mod 5
    <=> 7j ≡ 4-6 mod 5
    <=> 7j ≡ -2 mod 5
Resolve a congruence in python for big numbers is not simple. To achieve that, i wrote 2 functions. 

The first is the extended euclidean algorithm who give me the PGCD of two numbers a and b, and one pair of Bezout coefficient u et v like a\*u + b\*v = PGCD. 

The second use the first one to solve the congruence efficiently.

Indeed, after long documentation again, the principe is to find efficently the good coefficient for multiply to resume the factor of j.

7 and 5 are prime, so they are also prime themselfs. There PGCD is 1. 

I pass explaination, but finding Bezout coefficient allow us to simplify this :

    7j ≡ -2 mod 5
in this :

    j ≡ -2 * u mod 5
Bezout coefs u and v are equals to -2 and 3 So 

        j ≡ -2 * -2 mod 5
    <=> j ≡ 4 mod 5
Like j, we define k, because  j ≡ 4 mod 5 is exactly equivalent to j = 4 + 5k. We have :
    j ≡ 4 mod 5
<=> j = 4 + 5k
Substitue this value of j in the x representation :

        x = 7j + 6
    <=> x = 7(4 + 5k) + 6
    <=> x = 35k + 34
Substitute again in the last system value :

        x ≡ 1 mod 3
    <=> 35k + 34 ≡ 1 mod 3
    <=> 35k ≡ 1-34 mod 3
    <=> 35k ≡ -33 mod 3
Solve congruence again :

    <=> k ≡ 0 mod 3
Again, resume in equation : 
Let l like 

        k ≡ 0 mod 3
    <=> k = 0 + 3l
Substitute in the x representation again : 

        x = 35k + 34
    <=> x = 35(0 + 3l) + 34
    <=> x = 105l + 34
    <=> x ≡ 34 mod 105
Finally, we have guess our mysterious number 34. Indeed, if x < 105, then x = 34.

## Implementation

To be sure to find a solution, we have to choose the good system.

A good system is composed with primes numbers modulos.

    0 < x < Prime1 * Prime2 * 2 * 3 * 5
    0 < x < Prime1 * Prime2 * 30
To be certain to have a system that compute x, we can choose as modulos for asking the 2 generated primes numbers. System is :

    x ≡ Return_Prime1 mod Prime1
    x ≡ Return_Prime2 mod Prime2

Our system is missing one value. But, has we can try 30 times, why not use 31. It's prime and gave us the ability to be certain that the product of modulo is greater than x. We just can try all values of remainder between 0 and 29. Therefore, if the final remainder is 30, the program will fail. (~1 try each 30).